#!/bin/bash
read -p "Dime cantidad de litros: " litros
coste=0

if [ $litros -lt 50 ]; then
	coste=$(expr $coste + 20)
	total=$coste
	echo "$total€"
elif [ $litros -le 200 ]; then
	litros=$(expr $litros - 50)
	coste=$(expr $coste + 20)
	restoL=$(expr $litros \* 20)
	restoL=$(expr $restoL / 100)
	total=$(expr $coste + $restoL)
	echo "$total€"
elif [ $litros -gt 200 ]; then
	litros=$(expr $litros - 200)
	coste=$(echo "$litros * 0.10 + 20 + 150 * 0.20" | bc -l)
	echo "$coste€"
fi
