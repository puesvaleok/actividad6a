#!/bin/bash
read -p "Dime un número del 1 al 30: " numero

while [ $numero -le 0 -o $numero -gt 30 ]; do
	read -p "Dime un número del 1 al 30: " numero
done

dia=$(expr $numero / 7)
dia2=$(expr $numero - 7 \* $dia)
if [ $dia2 -eq 1 ]; then
	echo "Lunes"
elif [ $dia2 -eq 2 ]; then
	echo "Martes"
elif [ $dia2 -eq 3 ]; then
	echo "Miércoles"
elif [ $dia2 -eq 4 ]; then
	echo "Jueves"
elif [ $dia2 -eq 5 ]; then
	echo "Viernes"
elif [ $dia2 -eq 6 ]; then
	echo "Sabado"
else
	echo "Domingo"
fi


