#!/bin/bash

read -p "Introduce un numero para saber si el numero es par o impar: " numero

while [ $numero -le 0 ]; do
   echo "Este numero es igual o menor que 0"
   read -p "Introduce un número para saber si el número es par o impar: " numero
done
resto=$(expr $numero % 2)
if [ $resto -eq 0 ]; then
   echo “El numero $numero es par”
else
   echo “El numero $numero es impar”
fi
