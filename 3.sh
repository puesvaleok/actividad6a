#!/bin/bash
read -p "Dime tu nota: " nota
while [ $nota -lt 0 -o $nota -gt 10 ]; do
 echo "La nota tiene que ser mayor a 0 y menor a 11"
 read -p "Dime tu nota: " nota
done

if [ $nota -lt 5 ]; then 
 echo "Estas suspendido"

elif [ $nota -gt 5 -a $nota -le 6 ]; then
 echo "Bien"

elif [ $nota -ge 5 -a $nota -lt 6 ]; then
 echo "Estas Aprobado"

elif [ $nota -gt 6 -a $nota -le 8 ]; then
 echo "Notable"

elif [ $nota -gt 8 -a $nota -le 10 ]; then
 echo "Excelente"
fi

